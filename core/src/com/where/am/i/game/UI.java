package com.where.am.i.game;

import java.util.LinkedList;
import java.util.List;

public class UI {

	private int health;
	private int mana;
	private World world;
	private SoundController soundController;
	
	private List<isDeadListener> deadListeners;
	
	public interface isDeadListener {
        void notifyDead();			
    }
	
	public void registerDeadListener(isDeadListener l) {
        deadListeners.add(l);
    }
 
    private void notifyDeadListeners() {
        for(isDeadListener l : deadListeners) {
            l.notifyDead();
        }
    }
    
    private void registerPickHeartListener() {
    	world.getCarl().registerPickItemListener(new Carl.pickItemListener() {
			
			@Override
			public void notifyPickHeart() {
				// TODO Auto-generated method stub
				health++;
			}
			
			@Override
			public void notifyPickMana() {
				mana++;
			}
		});
    }

	public UI(World world) {
		health = 3;
		mana = 3;
		this.world = world;
		deadListeners = new LinkedList<isDeadListener>();
		this.soundController = world.getSound();
	}
	
	public void register() {
		registerPickHeartListener();
	}
	
	public void update() {
		if(isDead()) {
			notifyDeadListeners();
		}
	}

	public boolean isDead() {
		return health <= 0;
	}

	public void getInjured() {
		this.soundController = world.getSound();
		soundController.injured();
		health--;
	}

	public void getHealHealth() {
		if (health < 5)
			health++;
	}

	public void getHealMana() {
		if (mana < 5)
			mana++;
	}

	public void useMana() {
		mana--;
	}

	public int getHealth() {
		return health;
	}

	public int getMana() {
		return mana;
	}
}
