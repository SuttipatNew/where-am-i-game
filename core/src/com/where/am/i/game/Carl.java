package com.where.am.i.game;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.where.am.i.game.UI.isDeadListener;

public class Carl {

	private int SPEED = 5;
	private int GRAVITY = 8;
	private World world;
	private SoundController soundController;
	private LinkedList<Bullet> bulletList;
	private Rectangle carlRect;
	private Rectangle carlHead;

	private double attackDelayTime = 0;
	private double magicDelayTime = 0;

	enum Action {
		Run, Jump, Attack, Still, Magic
	};

	enum Direction {
		Left, Right
	};
	
	private void registerDeadListener() {
        ui.registerDeadListener(new UI.isDeadListener() {
            @Override
            public void notifyDead() {
                dying();
            }
        });
    }
	
	public interface pickItemListener {
		void notifyPickHeart();
		void notifyPickMana();
	}
	private List<pickItemListener> pickItemListeners;
	
	public void registerPickItemListener(pickItemListener l) {
        pickItemListeners.add(l);
    }
 
    private void notifyPickHeartListener() {
        for(pickItemListener l : pickItemListeners) {
            l.notifyPickHeart();
        }
    }
    
    private void notifyPickManaListener() {
    	for(pickItemListener l : pickItemListeners) {
            l.notifyPickMana();
        }
    }
    
	private Vector2 position;
	private Map map;
	private UI ui;

	private float jumpTarget = -1;
	private int jumpStep = -1;
	private double lastGetInjuredTime = 0;
	private double lastPositionX = 0;

	private int attackPower;

	private Action[] actions;
	private Direction currentDirection = Direction.Right;

	public Carl(int x, int y, World world) {
		position = new Vector2(x, y);
		this.world = world;
		this.map = world.getMap();
		this.ui = world.getUI();
		this.actions = new Action[4];
		attackPower = 20;
		pickItemListeners = new LinkedList<pickItemListener>();
		carlRect = new Rectangle();
		carlRect.setPosition(position.x - 20, position.y);
		carlRect.setSize(40, 50);
		carlHead = new Rectangle();
		carlHead.setSize(65,58);
		carlHead.setPosition(position.x - 30, position.y + 50);
	}

	public void init() {
		registerDeadListener();
		bulletList = world.getBulletList();
		this.soundController = world.getSound();
	}
	
	public void update() {
		if(!ui.isDead()) {
			int r = getRow();
			int c = getCol();
			updateHealth(r, c);
			updateMovement(r, c);
			updatePickHeart(r, c);
			updatePickMana(r, c);
			carlHead.setPosition(position.x - 30, position.y + 50);
			carlRect.setPosition(position.x - 20, position.y);
		} else {
			soundController.stopAll();
		}
	}

	public void updateHealth(int r, int c) {
		if ((map.hasTrapAt(r, c) || isInjuring) && lastGetInjuredTime == 0) {
			ui.getInjured();
			lastGetInjuredTime = currentTime();
			isInjuring = false;
		}
		else {
			isInjuring = false;
		}
		if (map.hasTrapAt(r, c)) {
			setNextAction(Action.Jump);
			setJumpHeight(150);
		}
		if (currentTime() - lastGetInjuredTime >= 1000) {
			pushTarget = -1;
			lastGetInjuredTime = 0;
		}
		if (pushTarget != -1) {
			pushed();
		}
	}

	public void updateMovement(int r, int c) {
		if(getCurrentAction() == Action.Attack || getCurrentAction() == Action.Magic) {
			if (getCurrentAction() == Action.Attack && currentTime() - attackDelayTime >= 200) {
				finishCurrentAction();
				attackDelayTime = 0;
			} else if (getCurrentAction() == Action.Magic && currentTime() - magicDelayTime >= 200) {
				finishCurrentAction();
				magicDelayTime = 0;
			}
		} else {
			finishCurrentAction();
		}
		Action currentAction = actions[1];
		if (currentAction == Action.Run || isNotInBlock()) {
			if (currentDirection == Direction.Left && canRun(Direction.Left)) {
				position.x -= SPEED;
			} else if (currentDirection == Direction.Right && canRun(Direction.Right)) {
				position.x += SPEED;
			}
		}
		if (currentAction == Action.Attack && attackDelayTime == 0) {
			soundController.whip();
			attackDelayTime = currentTime();
		}
		
		if (currentAction == Action.Magic && magicDelayTime == 0) {
			Bullet.Direction bulletDir = Bullet.Direction.Left;
			if(currentDirection == Direction.Right) {
				bulletDir = Bullet.Direction.Right;
			}
			if(ui.getMana() > 0) {
				soundController.magic();
				bulletList.add(new Bullet(position.x, position.y, bulletDir));
				ui.useMana();
			}
			magicDelayTime = currentTime();
		}
		jump();
		fixPosition(r, c);
		lastPositionX = position.x;
	}
	
	private void updatePickHeart(int r, int c) {
		if(map.hasHeartAt(r, c)) {
			notifyPickHeartListener();
			map.removeHeartAt(r, c);
		}
		if(map.hasHeartAt(r - 1, c)) {
			notifyPickHeartListener();
			map.removeHeartAt(r - 1, c);
		}
		if(map.hasHeartAt(r - 2, c)) {
			notifyPickHeartListener();
			map.removeHeartAt(r - 2, c);
		}
	}
	
	private void updatePickMana(int r, int c) {
		if(map.hasManaAt(r, c)) {
			notifyPickManaListener();
			map.removeManaAt(r, c);
		}
		if(map.hasManaAt(r - 1, c)) {
			notifyPickManaListener();
			map.removeManaAt(r - 1, c);
		}
		if(map.hasManaAt(r - 2, c)) {
			notifyPickManaListener();
			map.removeManaAt(r - 2, c);
		}
	}

	// About attack delay
	private double currentTime() {
		return System.currentTimeMillis();
	}

	// ---------------------------------------------------------------------------------------------

	// About jump and fall
	// -----------------------------------------------------------------------
	private void jump() {
		if (position.y < jumpTarget) {
			position.y += GRAVITY;
		} else {
			jumpTarget = -1;
			fall();
		}
	}

	private void fall() {
		if (!map.hasWallAt(getRow() + 1, getCol())) {
			position.y -= GRAVITY;
		} else {
			resetJumpStep();
		}
	}

	public int getJumpStep() {
		return jumpStep;
	}

	public void resetJumpStep() {
		jumpStep = -1;
	}

	public void setJumpHeight(int height) {
		soundController.jump();
		jumpTarget = (int) position.y + height;
		if (jumpStep < 1) {
			jumpStep++;
		}
	}

	public boolean isOnFloor() {
		int r = (int) (WhereAmI.HEIGHT - position.y) / WorldRenderer.BLOCK_SIZE;
		int c = (int) position.x / WorldRenderer.BLOCK_SIZE;
		return map.hasWallAt(r, c);
	}

	// -----------------------------------------------------------------------------

	// About fix position
	// ----------------------------------------------------------

	private boolean canRun(Direction direction) {
		int r = getRow();
		int c = getCol();
		int posDiff = 1;
		if (direction == Direction.Left)
			posDiff = -1;
		c += posDiff;
		boolean useWhenJumping = !map.hasWallAt(r + 1, c);
		if (isOnFloor()) {
			useWhenJumping = true;
		}
		return useWhenJumping && !map.hasWallAt(r, c) && !map.hasWallAt(r - 1, c) && !map.hasWallAt(r - 2, c);
	}

	private boolean isNotInBlock() {
		Action currentAction = getCurrentAction();
		return currentAction == Action.Still && position.x % WorldRenderer.BLOCK_SIZE != 0;
	}

	public void fixPosition(int r, int c) {
		int BLOCK_SIZE = WorldRenderer.BLOCK_SIZE;
		float x = position.x;
		float y = position.y;
		if (lastPositionX == x) {
			if (map.hasWallAt(r, c) || map.hasWallAt(r - 1, c) || map.hasWallAt(r - 2, c) || x % BLOCK_SIZE != 0) {
				if (currentDirection == Direction.Left && !map.hasWallAt(r, c + 1)) {
					position.x += SPEED;
				} else if (currentDirection == Direction.Right && !map.hasWallAt(r, c - 1)) {
					position.x -= SPEED;
				}
			}
		}
		if (y % BLOCK_SIZE != 0 && !map.hasWallAt(r - 1, c) && isOnFloor()) {
			position.y += SPEED;
		}
	}

	private float pushTarget = -1;
	private Direction pushDirection;

	public void setPush(float distance, Direction direction) {
		if(pushTarget == -1) {
			if (direction == Direction.Left) {
				pushTarget = position.x - distance;
			} else {
				pushTarget = position.x + distance;
			}
			pushDirection = direction;
		}
	}

	private void pushed() {
		if (this.getCurrentAction() == Action.Run)
			pushTarget = -1;
		if (pushTarget != -1) {
			int r = getRow();
			if (pushDirection == Direction.Left && position.x - pushTarget > 0) {
				int c = (int)Math.ceil(position.x / WorldRenderer.BLOCK_SIZE);
				if(!map.hasWallAt(r, c - 1) && !map.hasWallAt(r - 1, c - 1) && !map.hasWallAt(r - 2, c - 1)) {
					position.x -= SPEED * 2;
				}
			} else if (pushDirection == Direction.Right && pushTarget - position.x > 0) {
				int c = getCol();
				if(!map.hasWallAt(r, c + 1) && !map.hasWallAt(r - 1, c + 1) && !map.hasWallAt(r - 2, c + 1)) {
					position.x += SPEED * 2;
				}
			/*} else {
				pushTarget = -1;*/
			}
		}
	}

	// ---------------------------------------------------------------------------------------------

	// About setting actions
	// ----------------------------------------------------------------------
	public void setNextAction(Action action) {
		if (!isActionFull() || action == Action.Jump || action == Action.Attack) {
			if (actions[2] == null) {
				actions[2] = action;
			} else {
				actions[3] = action;
			}
		}
	}

	public boolean isActionFull() {
		return actions[2] != null && actions[3] != null;
	}

	public void finishCurrentAction() {
		for (int i = 0; i < actions.length; i++) {
			if (i < actions.length && i + 1 >= actions.length) {
				actions[i] = null;
			} else {
				actions[i] = actions[i + 1];
			}
		}
	}

	// ----------------------------------------------------------------------------

	public Vector2 getPosition() {
		return position;
	}

	public void setDirection(Direction dir) {
		currentDirection = dir;
	}

	public Direction getCurrentDirection() {
		return currentDirection;
	}

	public Action getCurrentAction() {
		return actions[1];
	}

	public Action getLastAction() {
		return actions[0];
	}

	public int getRow() {
		return (int) (WhereAmI.HEIGHT - position.y) / WorldRenderer.BLOCK_SIZE - 1;
	}

	public int getCol() {
		if (position.x % WorldRenderer.BLOCK_SIZE != 0 && currentDirection == Direction.Left)
			return (int) position.x / WorldRenderer.BLOCK_SIZE + 1;
		return (int) position.x / WorldRenderer.BLOCK_SIZE;
	}

	public void testHasWallAt(int r, int c) {
		System.out.println(map.hasWallAt(r, c));
	}

	public double getLastGetInjuredTime() {
		return lastGetInjuredTime;
	}

	private boolean isInjuring = false;

	public void getInjured() {
		isInjuring = true;
	}

	public int getAttackPower() {
		return attackPower;
	}
	
	private double lastTimeForDying = -1;
	private boolean goUp = false;
	private float positionYForDying = -1;
	public void dying()
	{
		if(lastTimeForDying == -1) {
			lastTimeForDying = System.currentTimeMillis();
			positionYForDying = position.y;
		}
		float height = 100;
		if(position.y < positionYForDying + height && !goUp) {
			position.y += GRAVITY;
		}
		else if(position.y >= positionYForDying + height) {
			goUp = true;
		}
		if(position.y > -30 && goUp) {
			position.y -= GRAVITY;
		}
	}
	
	public boolean isDead() {
		return ui.isDead();
	}
	
	public Rectangle getRect() {
		return carlRect;
	}
	
	public Rectangle getHead() {
		return carlHead;
	}
	
	public void setAttackPower(int n) {
		attackPower = n;
	}
}
