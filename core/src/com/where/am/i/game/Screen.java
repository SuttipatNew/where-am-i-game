package com.where.am.i.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class Screen extends ScreenAdapter {
	
	private SpriteBatch batch;
	private static final Texture gameOverScreen = new Texture("gameover.jpg");
	private static final Texture blank = new Texture("blank.png");
	private Texture img;
	private OrthographicCamera camera;
	private UI ui;
	private boolean died = false;
	private float opacity = .0f;
	private double lastOpacityTime = 0;
	private World world;
	private int pause;
	private boolean onMenu;
	private boolean instruction;
	private GameScreen gameScreen;
	FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("2005_iannnnnCTX-9001.ttf"));
	FreeTypeFontParameter parameter = new FreeTypeFontParameter();
	private BitmapFont font;

	private static final Texture menuScreen = new Texture("menu.jpg");
	private static final Texture instructionScreen = new Texture("instruction.jpg");
	private static final Texture pauseScreen = new Texture("pause.jpg");
	private static final Texture newGameNormal = new Texture("new-game-normal.png");
	private static final Texture newGameHilight = new Texture("new-game-hilight.png");
	private static final Texture instructionNormal = new Texture("instruction-normal.png");
	private static final Texture instructionHilight = new Texture("instruction-hilight.png");
	private static final Texture menuNormal = new Texture("menu-normal.png");
	private static final Texture menuHilight = new Texture("menu-hilight.png");
	private static final Texture resumeNormal = new Texture("resume-normal.png");
	private static final Texture resumeHilight = new Texture("resume-hilight.png");
	private static final Texture retryNormal = new Texture("retry-normal.png");
	private static final Texture retryHilight = new Texture("retry-hilight.png");
	private Texture newGameImg;
	private Texture instructionImg;
	private Texture menuImg;
	private Texture resumeImg;
	private Texture retryImg;
	private int score;
	private boolean keptScore;
	
	private void registerDeadListener() {
        ui.registerDeadListener(new UI.isDeadListener() {
            @Override
            public void notifyDead() {
                gameOver();
            }
        });
    }
	
	public Screen(SpriteBatch batch, GameScreen gameScreen, World world) {
		this.world = world;
		this.batch = batch;
		this.img = blank;
		this.gameScreen = gameScreen;
		this.newGameImg = newGameHilight;
		this.instructionImg = instructionNormal;
		this.resumeImg = resumeHilight;
		this.retryImg = retryNormal;
		this.menuImg = menuNormal;
		parameter.size = 35;
		font = generator.generateFont(parameter);
		font.setColor(Color.WHITE);
		font.getData().setScale(1.0f); 
		this.keptScore = false;
	}
	
	public void init(World world) {
		this.world = world;
		this.ui = world.getUI();
		this.camera = world.getCamera();
		registerDeadListener();
	}
	
	@Override
	public void render(float delta) {
		Color color = batch.getColor();
		if(died) {
			if(System.currentTimeMillis() - lastOpacityTime >= 50 && opacity < 1) {
				opacity += 0.05;
				lastOpacityTime = System.currentTimeMillis();
			}
			batch.setColor(color.r, color.g, color.b, opacity);
		}
		batch.begin();
		if(!onMenu) {
			batch.draw(img, camera.position.x - WhereAmI.WIDTH / 2, camera.position.y - WhereAmI.HEIGHT / 2);
			if(this.pause == 1) {
				batch.draw(resumeImg, camera.position.x - 300, camera.position.y - 60);
				batch.draw(retryImg, camera.position.x - 250, camera.position.y - 120);
				batch.draw(menuImg, camera.position.x - 200, camera.position.y - 180);
			}
			if(world.getCarl().isDead() && opacity >= 1) {
				font.draw(batch, "Score    " + this.score, camera.position.x - 80 , camera.position.y - 200);
			}
		} else if (onMenu){
			float x = 0;
			float y = 0;
			if(camera != null) {
				x = camera.position.x - WhereAmI.WIDTH/2;
				y = camera.position.y - WhereAmI.HEIGHT/2;
				camera.update();
				batch.setProjectionMatrix(camera.combined);
			}
			batch.draw(img, x, y);
			batch.draw(newGameImg, x + 100, y + 200);
			batch.draw(instructionImg, x + 150, y + 140);
		} else if (instruction) {
			float x = 0;
			float y = 0;
			if(camera != null) {
				x = camera.position.x - WhereAmI.WIDTH/2;
				y = camera.position.y - WhereAmI.HEIGHT/2;
				camera.update();
				batch.setProjectionMatrix(camera.combined);
			}
			batch.draw(img, x, y);
		}
		batch.end();
		batch.setColor(color.r, color.g, color.b, 1.0f);
	}
	
	private void gameOver() {
		if(onMenu || instruction) {
			return;
		}
		img = gameOverScreen;
		died = true;
		if(lastOpacityTime == 0) {
			lastOpacityTime = System.currentTimeMillis();
		}
		if(!keptScore) {
			this.score = world.getScore();
			keptScore = true;
		}
	}
	
	public void update() {
		this.onMenu = gameScreen.getOnMenu();
		this.pause = gameScreen.getPause();
		this.instruction = gameScreen.getInstruction();
		if(onMenu) {
			updateMenu();
			img = menuScreen;
		} else if (instruction){
			img = instructionScreen;
		} else if(pause == 0) {
			img = blank;
		} else if (pause == 1){
			updatePauseMenu();
			img = pauseScreen;
		}
	}
	
	public void updateMenu() {
		int selectMenu = gameScreen.getSelectMenu();
		if (selectMenu == 0) {
			newGameImg = newGameHilight;
			instructionImg = instructionNormal;
		} else if (selectMenu == 1) {
			newGameImg = newGameNormal;
			instructionImg = instructionHilight;
		}
	}
	
	public void updatePauseMenu() {
		int selectPauseMenu = gameScreen.getSelectPauseMenu();
		if (selectPauseMenu == 0) {
			resumeImg = resumeHilight;
			retryImg = retryNormal;
			menuImg = menuNormal;
		} else if (selectPauseMenu == 1) {
			resumeImg = resumeNormal;
			retryImg = retryHilight;
			menuImg = menuNormal;
		} else if (selectPauseMenu == 2) {
			resumeImg = resumeNormal;
			retryImg = retryNormal;
			menuImg = menuHilight;
		}
	}
}
