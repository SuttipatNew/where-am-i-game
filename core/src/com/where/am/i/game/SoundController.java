package com.where.am.i.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class SoundController {
	public static final Sound jumpSound = Gdx.audio.newSound(Gdx.files.internal("jump.wav"));
	public static final Sound change = Gdx.audio.newSound(Gdx.files.internal("change.mp3"));
	public static final Sound select = Gdx.audio.newSound(Gdx.files.internal("select.mp3"));
	public static final Sound bgm = Gdx.audio.newSound(Gdx.files.internal("bgm.mp3"));
	public static final Sound whip = Gdx.audio.newSound(Gdx.files.internal("whip.wav"));
	public static final Sound hit = Gdx.audio.newSound(Gdx.files.internal("hit.mp3"));
	public static final Sound magic = Gdx.audio.newSound(Gdx.files.internal("fireball.mp3")); 
	public static final Sound injure = Gdx.audio.newSound(Gdx.files.internal("injure.mp3")); 

	private World world;
	
	public SoundController(World world) {
		this.world = world;
	}
	
	public void jump() {
		jumpSound.play(.4f);
	}
	
	public void changeMenu() {
		change.play(.6f);
	}
	
	public void selectMenu() {
		select.play(0.6f);
	}
	
	public void background() {
		long id = bgm.play(.6f);
		bgm.setLooping(id, true);
	}
	
	public void whip() {
		whip.play(0.8f);
	}
	
	public void hitMonster() {
		hit.play(0.8f);
	}
	
	public void stopAll() {
		bgm.stop();
	}
	
	public void magic() {
		magic.play(1.0f);
	}
	
	public void injured() {
		injure.play(0.8f);
	}
	
}
