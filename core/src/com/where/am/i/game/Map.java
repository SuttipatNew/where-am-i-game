package com.where.am.i.game;

import java.util.Random;

public class Map {
	public static final String gameMap[] = {
			".....................................................................................................................................................................................",
			"##########...........................................................................................................................................#############################...",
			"##########...........................................................................................................................................#############################...",
			"##########...........................................................................................................................................#############################...",
			"##########...........................................................................................................................................#############################...",
			"##########...........................................................................................................................................#############################...",
			"##########...........................................................................................................................................#############################...",
			"##########...........................................................................................................................................#############################...",
			"########################............####..................................##...........................................................###########################################...",
			"##########################..........####.................................###........BB...................######......................#############################################...",
			"###################################################TTT#########################################################TTTT###############################################################...",
			"##################################################################################################################################################################################...",
			"##################################################################################################################################################################################...",
			"##################################################################################################################################################################################...",
			"##################################################################################################################################################################################...",
			"##################################################################################################################################################################################..." };

	private int height;
	private int width;
	private int heartLimit = 5;
	private int heartNb = 0;
	private int manaLimit = 3;
	private int manaNb = 0;
	private boolean[][] hearts;
	private boolean[][] mana;

	public Map() {
		height = gameMap.length;
		width = gameMap[0].length();
		findHearts();
		findMana();
	}
	
	private void findHearts() {
		hearts = new boolean[height][width];
		for(int i = 0; i < gameMap.length; i++) {
			for(int j = 0; j < gameMap[i].length(); j++) {
				hearts[i][j] = gameMap[i].charAt(j) == 'H';
			}
		}
	}
	
	private void findMana() {
		mana = new boolean[height][width];
		for(int i = 0; i < gameMap.length; i++) {
			for(int j = 0; j < gameMap[i].length(); j++) {
				mana[i][j] = gameMap[i].charAt(j) == 'M';
			}
		}
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public boolean hasWallAt(int r, int c) {
		if (r < 0 || r >= gameMap.length || c < 0 || c >= gameMap[r].length())
			return false;
		return gameMap[r].charAt(c) == '#' || gameMap[r].charAt(c) == 'B';
	}
	
	public boolean hasBoxAt(int r, int c) {
		if (r < 0 || r >= gameMap.length || c < 0 || c >= gameMap[r].length())
			return false;
		return gameMap[r].charAt(c) == 'B';
	}

	public boolean hasTrapAt(int r, int c) {
		if (r < 0 || r >= gameMap.length || c < 0 || c >= gameMap[r].length())
			return false;
		return gameMap[r].charAt(c) == 'T';
	}

	public boolean hasDotAt(int r, int c) {
		if (r < 0 || r >= gameMap.length || c < 0 || c >= gameMap[r].length())
			return false;
		return gameMap[r].charAt(c) == '.';
	}
	
	public boolean hasHeartAt(int r, int c) {
		if (r < 0 || r >= gameMap.length || c < 0 || c >= gameMap[r].length())
			return false;
		return hearts[r][c];
	}

	public String[] getMapString() {
		return gameMap;
	}
	
	public void removeHeartAt(int r, int c) {
		hearts[r][c] = false;
		heartNb--;
	}
	
	public boolean hasManaAt(int r, int c) {
		if (r < 0 || r >= gameMap.length || c < 0 || c >= gameMap[r].length())
			return false;
		return mana[r][c];
	}
	
	public void removeManaAt(int r, int c) {
		mana[r][c] = false;
		manaNb--;
	}
	
	private double lastTimeGenHeart = -1;
	public void heartGenerate(int min, int max, double interval) {
		Random rand = new Random();
		int prob = rand.nextInt(600);
		if((lastTimeGenHeart == -1 || System.currentTimeMillis() - lastTimeGenHeart >= interval) && prob < 1 && heartNb < heartLimit) {
			int c = rand.nextInt(max - min) + min;
			int r = gameMap.length - 1;
			while(hasWallAt(r, c) || hasTrapAt(r, c)) {
				r--;
			}
			r--;
			if(r >= 0) {
				hearts[r][c] = true;
				heartNb++;
			}
		}
		
	}
	public void manaGenerate(int min, int max, double interval) {
		Random rand = new Random();
		int prob = rand.nextInt(700);
		if((lastTimeGenHeart == -1 || System.currentTimeMillis() - lastTimeGenHeart >= interval) && prob < 1 && manaNb < manaLimit) {
			int c = rand.nextInt(max - min) + min;
			int r = gameMap.length - 1;
			while(hasWallAt(r, c) || hasTrapAt(r, c)) {
				r--;
			}
			r--;
			if(r >= 0) {
				mana[r][c] = true;
				manaNb++;
			}
		}
		
	}
}
