package com.where.am.i.game;

import java.util.LinkedList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class MonsterRenderer {

	private SpriteBatch batch;
	private LinkedList<Monster> monsterList;
	private ShapeRenderer shapeRenderer;

	public MonsterRenderer(SpriteBatch batch, LinkedList<Monster> monsterList) {
		this.batch = batch;
		this.monsterList = monsterList;
		shapeRenderer = new ShapeRenderer();
	}

	public void render() {
		batch.begin();
		for (int i = 0; i < monsterList.size(); i++) {
			monsterList.get(i).update();
			Vector2 position = monsterList.get(i).getPosition();
			float height = monsterList.get(i).getTexture().getHeight();
			float width = monsterList.get(i).getTexture().getWidth();
			Monster monster = monsterList.get(i);
			batch.draw(monster.getTexture(), position.x - width / 2, position.y);
			
		}
		batch.end();
	}
}
