package com.where.am.i.game;

import java.util.LinkedList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Bullet {
	private int speed = 8;
	private static final Texture bullet1LeftImg = new Texture("flame1-left.png");
	private static final Texture bullet2LeftImg = new Texture("flame2-left.png");
	private static final Texture bullet3LeftImg = new Texture("flame3-left.png");
	private static final Texture bullet1RightImg = new Texture("flame1-right.png");
	private static final Texture bullet2RightImg = new Texture("flame2-right.png");
	private static final Texture bullet3RightImg = new Texture("flame3-right.png");
	private Texture img;
	private Rectangle rect;
	
	enum Direction {
		Left, Right
	};
	
	enum STATE {
		ON, OFF
	};

	private Direction currentDirection;
	private Vector2 position;

	public Bullet(float x, float y, Direction dir) {
		position = new Vector2(x, y);
		this.currentDirection = dir;
		if(dir == Direction.Left) {
			this.img = bullet1LeftImg;
		} else {
			this.img = bullet1RightImg;
		}
		rect = new Rectangle();
		rect.setPosition(position.x + 15 , position.y + 30);
		rect.setSize(40, 20);
	}

	public void update() {
		move();
		swap();
		rect.setPosition(position.x + 15, position.y + 30);
	}
	
	private void swap() {
		if(currentDirection == Direction.Right) {
			if(img == bullet1RightImg) {
				img = bullet2RightImg;
			} else if(img == bullet2RightImg) {
				img = bullet3RightImg;
			} else if(img == bullet3RightImg) {
				img = bullet1RightImg;
			}
		} else {
			if(img == bullet1LeftImg) {
				img = bullet2LeftImg;
			} else if(img == bullet2LeftImg) {
				img = bullet3LeftImg;
			} else if(img == bullet3LeftImg) {
				img = bullet1LeftImg;
			}
		}
	}

	private void move() {
		if (currentDirection == Direction.Left) {
			position.x -= speed;
		} else {
			position.x += speed;
		}
	}
	
	public Vector2 getPostion() {
		return position;
	}
	
	public Texture getTexture() {
		return img;
	}
	
	public Rectangle getRect() {
		return rect;
	}
}
