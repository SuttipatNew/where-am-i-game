package com.where.am.i.game;

import java.util.LinkedList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.where.am.i.game.Carl.Action;
import com.where.am.i.game.Carl.Direction;

public class World {
	
	private double monsterGenInterval = 1000;
	private double monsterGenProb = 0;
	
//	private WhereAmI whereAmIGame;
	private Carl carl;
	private Map map;
	private UI ui;
	// private Monster[] monsters;
	private OrthographicCamera camera;
	private SoundController soundController;
	private LinkedList<Bullet> bulletList;
	private LinkedList<Monster> monsterList;
	private double lastTimeGenMonster = -1;
	private int score = 0;
	private GameScreen gameScreen;
	private boolean godMode;
	private boolean supFB;

	public World(WhereAmI whereAmIGame, GameScreen gameScreen) {
		this.gameScreen = gameScreen;
		camera = new OrthographicCamera();
//		this.whereAmIGame = whereAmIGame;
		map = new Map();
		this.ui = new UI(this);
		carl = new Carl(1200, 200, this);
		bulletList = new LinkedList<Bullet>();
		soundController = new SoundController(this);
		monsterList = new LinkedList<Monster>();
		godMode = false;
		supFB = false;
		init();
	}

	public void init() {
		carl.init();
		ui.register();
	}

	public Carl getCarl() {
		return this.carl;
	}

	public void update(float delta) {
		ui.update();
		carl.update();
		if(!carl.isDead() && !gameScreen.getInstruction()) {
			updateBullet();
			bulletAndMonsterCollisionCheck();
			int max = Map.gameMap[0].length() - 2 * 10;
			map.heartGenerate(10, max, 2000);
			map.manaGenerate(10, max, 2000);
			monsterGenerator(monsterGenInterval);
			removeBlankMonster();
			monsterGenProb = (score / 320) + 2;
		}
	}

	public Map getMap() {
		return map;
	}

	public UI getUI() {
		return ui;
	}

	public LinkedList<Monster> getMonsterList() {
		return this.monsterList;
	}

	public OrthographicCamera getCamera() {
		return camera;
	}

	public SoundController getSound() {
		return soundController;
	}

	public LinkedList<Bullet> getBulletList() {
		return bulletList;
	}

	private void updateBullet() {
		double width = WhereAmI.WIDTH;
		int size = bulletList.size();
		for (int i = 0; i < size; i++) {
			Bullet bullet = bulletList.get(i);
			bullet.update();
			Vector2 pos = bullet.getPostion();
			if (pos.x > camera.position.x + width / 2 || pos.x < camera.position.x - width / 2) {
				if(!supFB) {
					bulletList.remove(bullet);
					i--;
					size = bulletList.size();
				}
			}
		}
	}

	private void bulletAndMonsterCollisionCheck() {
		for (Monster monster : monsterList) {
			Rectangle mRect = monster.getRect();
			for (int i = 0; i < bulletList.size(); i++) {
				Rectangle bRect = bulletList.get(i).getRect();
				if(mRect.overlaps(bRect)) {
					if(!supFB) {
						bulletList.remove(i);
						i--;
					}
					monster.getInjured(carl.getAttackPower(), carl.getCurrentDirection());
				}
			}
		}
	}
	
	private void monsterGenerator(double interval) {
		if(monsterList.size() > 100) {
			return;
		}
		Random rand = new Random();
		int prob = rand.nextInt(300);
		if((lastTimeGenMonster == -1 || System.currentTimeMillis() - lastTimeGenMonster >= interval) && prob < monsterGenProb) {
			int max = map.getWidth() - 32;
			int min = 10;
			int c = rand.nextInt(max - min) + min;
			int r = Map.gameMap.length - 1;
			while(map.hasWallAt(r, c) || map.hasTrapAt(r, c)) {
				r--;
			}
			if(r >= 0 && Math.abs(c - carl.getCol()) > 4) {
				int x = c * WorldRenderer.BLOCK_SIZE;
				int y = (map.getHeight() - r) * WorldRenderer.BLOCK_SIZE;
				monsterList.add(new Monster(x, y, Monster.Type.Slime, this));
			}
		}
	}
	
	private void removeBlankMonster() {
		for(int i = 0; i < monsterList.size(); i++) {
			Monster monster = monsterList.get(i);
			if(monster.getHealth() <= 0) {
				monsterList.remove(monster);
				i--;
			}
		}
	}
	
	public void increaseScore(int s) {
		this.score += s;
	}
	
	public int getScore() {
		return score;
	}
	
	public void toggleGodMode() {
		if(godMode) {
			godMode = false;
		} else {
			godMode = true;
		}
	}
	
	public boolean isIngodMode() {
		return godMode;
	}

	public void toggleSuperFireBall() {
		if(!supFB) {
			supFB = true;
		} else {
			supFB = false;
		}
	}
}
