package com.where.am.i.game;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

public class WorldRenderer extends ScreenAdapter {

	public static final int BLOCK_SIZE = 40;
//	private WhereAmI whereAmIGame;
//	private World world;
	private CarlRenderer carlRenderer;
	private SpriteBatch batch;
	private Carl carl;
	private Texture background;
	private Map map;
	private MapRenderer mapRenderer;
	private OrthographicCamera camera;
	private UIRenderer uiRenderer;
	private LinkedList<Monster> monsterList;
	private MonsterRenderer monsterRenderer;
	private LinkedList<Bullet> bulletList;
//	private ShapeRenderer shapeRenderer;

	public WorldRenderer(WhereAmI whereAmIGame, World world) {
//		this.whereAmIGame = whereAmIGame;
//		this.world = world;
		this.batch = whereAmIGame.batch;
		this.carl = world.getCarl();
		this.carlRenderer = new CarlRenderer(batch, carl);
		this.background = new Texture("background2.jpg");
		this.map = world.getMap();
		camera = world.getCamera();
		camera.setToOrtho(false, 800, 600);
		this.mapRenderer = new MapRenderer(batch, map, camera);
		this.uiRenderer = new UIRenderer(batch, world, world.getUI());
		this.monsterList = world.getMonsterList();
		this.monsterRenderer = new MonsterRenderer(batch, monsterList);
		camera.position.y = carl.getPosition().y + 180;
		this.bulletList = world.getBulletList();
//		this.shapeRenderer = new ShapeRenderer();
	}

	@Override
	public void render(float delta) {
		camera.position.x = carl.getPosition().x;
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(background, camera.position.x - 400, camera.position.y - 300);
		batch.end();
		monsterRenderer.render();
		mapRenderer.render();
		drawBullet();
		carlRenderer.render();
		uiRenderer.render();
		
//		shapeRenderer.setProjectionMatrix(camera.combined);
//		shapeRenderer.begin(ShapeType.Line);
//		shapeRenderer.setColor(1, 1, 0, 1);
//		shapeRenderer.rect(carl.getRect().x, carl.getRect().y, carl.getRect().width, carl.getRect().height);
//		shapeRenderer.rect(carl.getHead().x, carl.getHead().y, carl.getHead().width, carl.getHead().height);
//		for(Monster monster : monsterList) {
//			Rectangle rect = monster.getRect();
//			shapeRenderer.rect(rect.x, rect.y, rect.width, rect.height);
//		}
//		for(Bullet bullet : bulletList) {
//			Rectangle rect = bullet.getRect();
//			shapeRenderer.rect(rect.x, rect.y, rect.width, rect.height);
//		}
//		shapeRenderer.end();
	}
	
	private void drawBullet() {
		int size = bulletList.size();
		batch.begin();
		for(int i = 0; i < size; i++) {
			Bullet bullet = bulletList.get(i);
			batch.draw(bullet.getTexture(), bullet.getPostion().x, bullet.getPostion().y);
		}
		batch.end();
	}
}
