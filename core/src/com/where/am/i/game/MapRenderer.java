package com.where.am.i.game;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MapRenderer {
	private Map map;
	private SpriteBatch batch;
	private static final Texture ground = new Texture("grass-center-fix.png");
	private static final Texture grass_left = new Texture("grass-left-fix.png");
	private static final Texture grass_left_down = new Texture("grass-left-fix-down.png");
	private static final Texture grass_right = new Texture("grass-right-fix.png");
	private static final Texture grass_right_down = new Texture("grass-right-fix-down.png");
	private static final Texture grass_mid = new Texture("grass-mid-fix.png");
	private static final Texture grass = new Texture("grass.png");
	private static final Texture grass_down = new Texture("grass-down.png");
	private static final Texture heartImg = new Texture("heart.png");
	private static final Texture manaImg = new Texture("mana.png");
	private static final Texture boxImg = new Texture("box.png");
	private Texture trapImg;
	private OrthographicCamera camera;

	private static Texture dot = new Texture("dot.jpg");

	public MapRenderer(SpriteBatch batch, Map map, OrthographicCamera camera) {
		this.map = map;
		this.batch = batch;
		trapImg = new Texture("trap.png");
		this.camera = camera;
	}

	public void render() {
		batch.begin();
		for (int i = 0; i < map.getHeight(); i++) {
			for (int j = 0; j < map.getWidth(); j++) {
				int x = j * WorldRenderer.BLOCK_SIZE;
				int y = WhereAmI.HEIGHT - (i * WorldRenderer.BLOCK_SIZE) - WorldRenderer.BLOCK_SIZE;
				if (map.hasBoxAt(i, j)) {
					batch.draw(boxImg, x - 20, y);
				} else if (map.hasWallAt(i, j)) {
					if (i > 0 && (!map.hasWallAt(i - 1, j) || map.hasBoxAt(i - 1, j))) {
						if(j > 0 && j < map.getWidth() - 1 && !map.hasWallAt(i, j - 1) && !map.hasWallAt(i, j + 1)) {
							if(i > 0 && !map.hasDotAt(i + 1, j)) {
								batch.draw(grass, x - 20, y);
							} else {
								batch.draw(grass_down, x - 20, y);
							}
						} else if(j > 0 && !map.hasWallAt(i, j - 1)) {
							if(i < map.getHeight() - 1 && map.hasWallAt(i + 1, j)) {
								batch.draw(grass_left_down, x - 20, y);
							} else {
								batch.draw(grass_left, x - 20, y);
							}
						} else if(j <  map.getWidth() - 1 && !map.hasWallAt(i, j + 1)) {
							if(i < map.getHeight() - 1 && map.hasWallAt(i + 1, j)) {
								batch.draw(grass_right_down, x - 20, y);
							} else {
								batch.draw(grass_right, x - 20, y);
							}
						} else {
							batch.draw(grass_mid, x - 20, y);
						}
						// batch.draw(dot, x, y);
					} else
						batch.draw(ground, x - 20, y);
				} else if (map.hasTrapAt(i, j)) {
					batch.draw(trapImg, x - (WorldRenderer.BLOCK_SIZE / 2), y);
				} else if (map.hasHeartAt(i, j)) {
					batch.draw(heartImg, x - (heartImg.getWidth() / 2), y);
				} else if (map.hasManaAt(i, j)) {
					batch.draw(manaImg, x - (manaImg.getWidth() / 2), y);
				}
			}
		}
		batch.end();
	}
}
