package com.where.am.i.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class UIRenderer {

	private SpriteBatch batch;
	private World world;
	private Carl carl;
	private UI ui;
	private Texture heart;
	private Texture manaImg;
	private int health;
	private int mana;
	private OrthographicCamera UICamera;
	private BitmapFont font;
	FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("2005_iannnnnCTX-9001.ttf"));
	FreeTypeFontParameter parameter = new FreeTypeFontParameter();

	public UIRenderer(SpriteBatch batch, World world, UI ui) {
		this.batch = batch;
		this.world = world;
		this.carl = world.getCarl();
		this.ui = ui;
		this.heart = new Texture("heart.png");
		this.manaImg = new Texture("mana.png");
		this.UICamera = new OrthographicCamera();
		this.UICamera.setToOrtho(false, 800, 600);
		UICamera.position.y = carl.getPosition().y + 180;
		parameter.size = 25;
		font = generator.generateFont(parameter);
		font.setColor(Color.BLACK);
		font.getData().setScale(1.0f);
	}

	public void render() {
		update();
		UICamera.position.x = carl.getPosition().x;
		// UICamera.position.y = carl.getPosition().y + 180;
		UICamera.update();
		batch.begin();

		for (int i = 0, posX = 380; i < health; i++, posX -= 40) {
			batch.draw(heart, UICamera.position.x - posX, UICamera.position.y + 250);
		}
		for (int i = 0, posX = 374; i < mana; i++, posX -= 40) {
			batch.draw(manaImg, UICamera.position.x - posX, UICamera.position.y + 220);
		}

		int score = world.getScore();
		font.draw(batch, "Score    " + score, UICamera.position.x + 220 , UICamera.position.y + 270);

		batch.end();

	}

	public void update() {
		health = ui.getHealth();
		mana = ui.getMana();
	}
}
