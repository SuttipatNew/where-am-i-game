package com.where.am.i.game;

import java.util.LinkedList;
import java.util.Random;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.where.am.i.game.Carl.Action;
import com.where.am.i.game.Carl.Direction;

public class Monster {

	private static final int injuredDelayTime = 400;
	private static final int Gravity = 5;
	
	public enum Type {
		Slime
	};

	enum Direction {
		Left, Right
	};

	public static final int slimeSpeed = 2;
	public static int pushSpeed = 10;
	private Vector2 position;
	private Carl carl;
	private Map map;
	private World world;
	private double health;
	private Type type;
	private Texture img;
	private static Texture slimeLeftImg = new Texture("slime-left.png");
	private static Texture slimeRightImg = new Texture("slime-right.png");
	private static Texture blank = new Texture("blank.png");
	private double lastGetInjuredTime = 0;
	private double start, end;
	private Direction currentDirection;
	private static final double slimeHealth = 50;
	private Vector2 lastPosition;
	private double pushTarget = -1;
	private Direction pushDirection;
	private LinkedList<Monster> monsterList;
	private double jumpTarget = -1;
	private Rectangle rect;
	private SoundController soundController;
	
	private double currentTime() {
		return System.currentTimeMillis();
	}

	public Monster(int x, int y, Type type, World world) {
		position = new Vector2(x, y);
		lastPosition = new Vector2(x, y);
		this.world = world;
		this.map = world.getMap();
		this.type = type;
		if (type == Type.Slime) {
			this.img = Monster.slimeLeftImg;
			this.health = slimeHealth;
			this.start = x - WorldRenderer.BLOCK_SIZE * 5;
			this.end = x + WorldRenderer.BLOCK_SIZE * 5;
			this.currentDirection = Direction.Left;
		}
		this.carl = world.getCarl();
		this.map = world.getMap();
		monsterList = world.getMonsterList();
		rect = new Rectangle();
		rect.setPosition(position.x - 25, position.y);
		rect.setSize(50, 40);
		this.soundController = world.getSound();
	}

	public void update() {
		if(pushTarget == -1 && lastGetInjuredTime == 0) {
			move(type);
			alwaysJump();
		}
		checkOnGround();
		checkCollision();
		checkGetInjured();
		push();
		fixPosition(getRow(), getCol(currentDirection));
		rect.setPosition(position.x - 25, position.y);
		
		Random rand = new Random();
		boolean hasTrapInFront = false;
		if(currentDirection == Direction.Left) {
			for(int i = getRow() + 1; i < map.getHeight(); i++) {
				if(map.hasTrapAt(i, getCol(currentDirection) - 1)){
					hasTrapInFront = true;
					break;
				}
			}
		} else {
			for(int i = getRow() + 1; i < map.getHeight(); i++) {
				if(map.hasTrapAt(i, getCol(currentDirection) + 1)){
					hasTrapInFront = true;
					break;
				}
			}
		}
		if(hasTrapInFront) {
			int n = rand.nextInt(10);
			if(n < 8) {
				if(isOnFloor()) {
					int jumpHeight = rand.nextInt(40) + 180;
					jumpTarget = position.y + jumpHeight;
				}
			}
		}
	}
	
	private void checkCollision() {
		if ((rect.overlaps(carl.getRect()) || rect.overlaps(carl.getHead())) && !world.isIngodMode()) {
			carl.getInjured();
			if(lastPosition.x < position.x) {
				carl.setPush(80, Carl.Direction.Right);
			} else if(lastPosition.x > position.x){
				carl.setPush(80, Carl.Direction.Left);
			}
		}
	}
	
	private void checkGetInjured() {
		if (carl.getCurrentAction() == Action.Attack && lastGetInjuredTime == 0) {
			blinkToggle = 0;
			float distance;
			if (carl.getCurrentDirection() == Carl.Direction.Left) {
				distance = carl.getPosition().x - position.x;
			} else {
				distance = position.x - carl.getPosition().x;
			}
			boolean inRangeY = position.y >= carl.getPosition().y && position.y < carl.getPosition().y + 80;
			if (distance <= 100 && distance > 0 && inRangeY) {
				getInjured(carl.getAttackPower(), carl.getCurrentDirection());
			}
		} else if (lastGetInjuredTime != 0) {
			blink(this.type, 300);
		}
		if (isLastInjuredTimerOver(injuredDelayTime)) {
			lastGetInjuredTime = 0;
			resetImg(this.type);
		}
	}
	
	public void setPush(int distance, Direction direction) {
		if(pushTarget == -1) {
			if (direction == Direction.Left) {
				pushTarget = position.x - distance;
			} else {
				pushTarget = position.x + distance;
			}
			pushDirection = direction;
		}
	}
	
	private void push() {
		int SPEED = getSpeed(type);
		if (pushTarget != -1) {
			int r = getRow();
			if (pushDirection == Direction.Left && position.x - pushTarget > 0) {
				int c = (int)Math.ceil(position.x / WorldRenderer.BLOCK_SIZE);
				if(!map.hasWallAt(r, c - 1)) {
					position.x -= pushSpeed;
				}
			} else if (pushDirection == Direction.Right && pushTarget - position.x > 0) {
				int c = getCol(currentDirection);
				if(!map.hasWallAt(r, c + 1)) {
					position.x += pushSpeed;
				}
			} else {
				pushTarget = -1;
			}
		}
		if(pushDirection == Direction.Left && !canMove(getRow(), getCol(currentDirection) - 1)) {
			pushTarget = -1;
		} else if(pushDirection == Direction.Right && !canMove(getRow(), getCol(Direction.Left) + 1)) {
			pushTarget = -1;
			
		}
	}
	
	public int getSpeed(Type type) {
		if(type == Type.Slime) {
			return slimeSpeed;
		}
		return 0;
	}

	public void move(Type type) {
		int r = getRow();
		int c = getCol(currentDirection);
		lastPosition.x = position.x;
		lastPosition.y = position.y;
		if (type == Type.Slime) {
			if (currentDirection == Direction.Left && !map.hasWallAt(r, c - 1) && !hasMonsterAt(getRow(), getCol(currentDirection) - 1)) {
				position.x -= slimeSpeed;
				img = slimeLeftImg;
			} else if (currentDirection == Direction.Right && !map.hasWallAt(r, c + 1) && !hasMonsterAt(getRow(), getCol(currentDirection) + 1)) {
				position.x += slimeSpeed;
				img = slimeRightImg;
			}
			if (position.x < carl.getPosition().x) {
				currentDirection = Direction.Right;
			} else if (position.x > carl.getPosition().x) {
				currentDirection = Direction.Left;
			}
		}
		
	}

	public void getInjured(int attackPower, Carl.Direction carlDirection) {
		soundController.hitMonster();
		this.health -= attackPower;


		Direction pushDirection = Direction.Left;
		if(carl.getCurrentDirection() == Carl.Direction.Right) {
			pushDirection = Direction.Right;
		}
		setPush(120, pushDirection);
		lastGetInjuredTime = currentTime();
		
		Random rand = new Random();
		if(health <= 0) {
			world.increaseScore(rand.nextInt(10)+80);
		}
	}

	public Vector2 getPosition() {
		return this.position;
	}

	public Texture getTexture() {
		return img;
	}

	public void removeIfDead() {
		if (isDead()) {
			img = blank;
		}
	}

	public boolean isDead() {
		return health <= 0;
	}

	private boolean isLastInjuredTimerOver(double timeInterval) {
		return currentTime() - lastGetInjuredTime >= timeInterval;
	}

	private int blinkToggle = 0;

	private void blink(Type type, double timeInterval) {
		if (blinkToggle == 0) {
			img = blank;
			blinkToggle++;
		} else {
			if (type == Type.Slime) {
				if(currentDirection == Direction.Left) {
					img = Monster.slimeLeftImg;
				}
				else {
					img = Monster.slimeRightImg;
				}
			}
			blinkToggle--;
		}
	}

	private void resetImg(Type type) {
		if (type == Type.Slime) {
			if(currentDirection == Direction.Left) {
				img = Monster.slimeLeftImg;
			}
			else {
				img = Monster.slimeRightImg;
			}
		}
	}

	public int getRow() {
		return (int) (WhereAmI.HEIGHT - position.y) / WorldRenderer.BLOCK_SIZE - 1;
	}

	public int getCol(Direction dir) {
		int fix = 0;
		if(dir == Direction.Left) {
			if(position.x % WorldRenderer.BLOCK_SIZE != 0) {
				fix = 1;
			}
		}
		return (int) position.x / WorldRenderer.BLOCK_SIZE + fix;
	}

	public Direction getDirection() {
		return this.currentDirection;
	}
	
	public boolean isOnFloor() {
		int r = (int) (WhereAmI.HEIGHT - position.y) / WorldRenderer.BLOCK_SIZE;
		int c = (int) position.x / WorldRenderer.BLOCK_SIZE;
		return map.hasWallAt(r, c);
	}

	public void checkOnGround() {
		if(!isOnFloor() && jumpTarget == -1) {
			position.y -= Gravity;
		}
		if(map.hasTrapAt(getRow(), getCol(currentDirection))) {
			health = 0; 
			world.increaseScore(20);
		}
	}
	
	public void fixPosition(int r, int c) {
		int BLOCK_SIZE = WorldRenderer.BLOCK_SIZE;
		float x = position.x;
		float y = position.y;
		if (lastPosition.x == x) {
			if (map.hasWallAt(r, c) || map.hasWallAt(r - 1, c) || map.hasWallAt(r - 2, c) || x % BLOCK_SIZE != 0) {
				if (currentDirection == Direction.Left && !map.hasWallAt(r, c + 1)) {
					position.x += slimeSpeed;
				} else if (currentDirection == Direction.Right && !map.hasWallAt(r, c - 1)) {
					position.x -= slimeSpeed;
				}
			}
		}
		if (y % BLOCK_SIZE != 0 && !map.hasWallAt(r - 1, c) && isOnFloor()) {
			position.y += slimeSpeed;
		}
	}
	
	public boolean hasMonsterAt(int r, int c) {
		for(Monster m : monsterList) {
			if(m.getRow() == r && m.getCol(m.getDirection()) == c && m != this) {
				return true;
			}
		}
		return false;
	}
	
	public double getHealth() {
		return health;
	}
	
	public void alwaysJump() {
		Random rand = new Random();
		if(rand.nextInt(100) < 4) {
			if(isOnFloor() && jumpTarget == -1) {
				int jumpHeight = rand.nextInt(40) + 60;
				jumpTarget = position.y + jumpHeight;
			}
		}
		if(position.y < jumpTarget) {
			position.y += Gravity;
		} else {
			jumpTarget = -1;
		}
	}
	
	private boolean canMove(int r, int c) {
		return !map.hasWallAt(r, c) && !hasMonsterAt(r, c);
	}
	
	public Rectangle getRect() {
		return rect;
	}
}
