package com.where.am.i.game;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.where.am.i.game.Carl.Action;
import com.where.am.i.game.Carl.Direction;
import com.where.am.i.game.Monster.Type;

public class CarlRenderer {

	private Texture carlImg;

	public static Texture standLeft = new Texture("carl-stand-left.png");
	public static Texture standRight = new Texture("carl-stand-right.png");
	public static Texture run1Left = new Texture("carl-run-1-left.png");
	public static Texture run1Right = new Texture("carl-run-1-right.png");
	public static Texture run2Left = new Texture("carl-run-2-left.png");
	public static Texture run2Right = new Texture("carl-run-2-right.png");
	public static Texture attackLeft = new Texture("carl-fight-left-fix.png");
	public static Texture attackRight = new Texture("carl-fight-right-fix.png");
	public static Texture magicLeft = new Texture("carl-magic-left.png");
	public static Texture magicRight = new Texture("carl-magic-right.png");
	public static Texture blank = new Texture("blank.png");

	private SpriteBatch batch;
	private Carl carl;
	private int blinkTime = -1;

	public CarlRenderer(SpriteBatch batch, Carl carl) {
		this.batch = batch;
		this.carl = carl;
		this.carlImg = standRight;
	}

	public void render() {
		Vector2 carlPosition = carl.getPosition();
		update();
		batch.begin();
		batch.draw(carlImg, carlPosition.x - carlImg.getWidth() / 2, carlPosition.y);
		// batch.draw(new Texture("dot.jpg"), carlPosition.x, carlPosition.y);
		batch.end();
	}

	public void update() {
		Direction currentDirection = carl.getCurrentDirection();
		if (!carl.isOnFloor()) {
			if (currentDirection == Direction.Left) {
				carlImg = run1Left;
			} else {
				carlImg = run1Right;
			}
		} else if (carl.getCurrentAction() == Action.Still) {
			if (currentDirection == Direction.Left) {
				carlImg = standLeft;
			} else {
				carlImg = standRight;
			}
		} else if (carl.getCurrentAction() == Action.Run) {
			if (carl.getLastAction() != Action.Run) {
				swapRound = 0;
				timeForSwapRunImg = 0;
			}
			swapRunImg(200, currentDirection);
		}

		if (carl.getCurrentAction() == Action.Attack) {
			if (currentDirection == Direction.Left) {
				carlImg = attackLeft;
			} else {
				carlImg = attackRight;
			}
		}
		
		if (carl.getCurrentAction() == Action.Magic) {
			if (currentDirection == Direction.Left){
				carlImg = magicLeft;
			} else {
				carlImg = magicRight;
			}
		}

		if (carl.getLastGetInjuredTime() != 0) {
			blink(300);
		}
		
		if(carl.isDead()) {
			if (currentDirection == Direction.Left) {
				carlImg = run1Left;
			} else {
				carlImg = run1Right;
			}
		}
	}

	// blink
	// -------------------------------------------------------------------------
	private int blinkToggle = 0;

	private void blink(double timeInterval) {
		if (blinkToggle == 0) {
			carlImg = blank;
			blinkToggle++;
		} else {
			blinkToggle--;
		}
	}
	// ------------------------------------------------------------------------------

	// for swap run img
	// -------------------------------------------------------------
	private double currentTime() {
		return System.currentTimeMillis();
	}

	private double timeForSwapRunImg = 0;
	private int swapRound = 0;
	private Texture imgInThisInterval;

	private void swapRunImg(double timeInterval, Direction currentDirection) {
		if (timeForSwapRunImg == 0 || currentTime() - timeForSwapRunImg >= timeInterval) {
			if (currentDirection == Direction.Left) {
				if (swapRound == 0)
					carlImg = run1Left;
				else if (swapRound == 1)
					carlImg = standLeft;
				else if (swapRound == 2)
					carlImg = run2Left;
				else if (swapRound == 3) {
					carlImg = standLeft;
					swapRound = -1;
				}
			} else {
				if (swapRound == 0)
					carlImg = run1Right;
				else if (swapRound == 1)
					carlImg = standRight;
				else if (swapRound == 2)
					carlImg = run2Right;
				else if (swapRound == 3) {
					carlImg = standRight;
					swapRound = -1;
				}
			}
			imgInThisInterval = carlImg;
			swapRound++;
			timeForSwapRunImg = currentTime();
		}
		carlImg = imgInThisInterval;
	}
	// -------------------------------------------------------------------------------------------------
}
