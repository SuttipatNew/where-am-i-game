package com.where.am.i.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.where.am.i.game.Carl.Action;
import com.where.am.i.game.Carl.Direction;

// alt-right - god mode
// ctrl-right - super power
// shift-right - supwer fire ball

public class GameScreen extends ScreenAdapter {

	private WhereAmI whereAmIGame;
	private World world;
	private WorldRenderer worldRenderer;
	private SpriteBatch batch;
	private Carl carl;
	private Screen screen;
	private int pause;
//	private Texture newGameImg;
//	private Texture instructionImg;
	private boolean onMenu;
	private boolean instruction;
	private int selectMenu;
	private int selectPauseMenu;
	private static final int totalPauseMenu = 3;
	private static final int totalMenu = 2;
	private SoundController soundController;

	public GameScreen(WhereAmI whereAmIGame) {
		this.world = new World(whereAmIGame, this);
		this.whereAmIGame = whereAmIGame;
		this.batch = whereAmIGame.batch;
//		this.newGameImg = newGameNormal;
//		this.instructionImg = blank;
		this.screen = new Screen(batch, this, world);
		selectMenu = 0;
		pause = 1;
		onMenu = true;
		instruction = false;
		this.soundController = world.getSound();
		soundController.background();
		// initGame();
	}

	public void initGame(boolean reSound) {
		if(reSound) {
			soundController.stopAll();
		}
		this.world = new World(whereAmIGame, this);
		this.worldRenderer = new WorldRenderer(whereAmIGame, world);
		this.carl = world.getCarl();
		this.screen.init(world);
		pause = 0;
		this.soundController = world.getSound();
		if(reSound) {
			soundController.background();
		}
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(100, 100, 100, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		updateKeyPress();
		screen.update();
		if(onMenu) {
			menuController();
		} else if (!onMenu && pause == 0) {
			world.update(delta);
			updateCarlAction();
			worldRenderer.render(delta);
		} else if (pause == 1) {
			pauseMenuController();
		}
		screen.render(delta);
	}

	public boolean isPressed(int key) {
		return Gdx.input.isKeyPressed(key);
	}

	public boolean isJustPressed(int key) {
		return Gdx.input.isKeyJustPressed(key);
	}

	// About key press
	// ----------------------------------------------------------------------------
	private double lastPressUpTime = 0;
	private double lastPressSpaceTime = 0;
	private double lastPressATime = 0;

	private double currentTime() {
		return System.currentTimeMillis();
	}

	public void updateCarlAction() {
		
		if(isJustPressed(Keys.CONTROL_RIGHT)) {
			if(carl.getAttackPower() == 20) {
				carl.setAttackPower(100);
			} else {
				carl.setAttackPower(20);
			}
		}
		
		if(isJustPressed(Keys.ALT_RIGHT)) {
			world.toggleGodMode();
		}
		
		if(isJustPressed(Keys.SHIFT_RIGHT)) {
			world.toggleSuperFireBall();
		}
		
		if (isJustPressed(Keys.X) && (carl.isOnFloor() || carl.getJumpStep() < 1)) {
			carl.setJumpHeight(100);
			lastPressUpTime = currentTime();
		}
		if (isJustPressed(Keys.Z) && isLastPressTimeOver(Keys.Z, 300)) {
			carl.setNextAction(Action.Attack);
			lastPressSpaceTime = currentTime();
		}
		if (isJustPressed(Keys.A) && isLastPressTimeOver(Keys.A, 300)) {
			carl.setNextAction(Action.Magic);
			lastPressATime = currentTime();
		}
		if (isPressed(Keys.LEFT) && isPressed(Keys.RIGHT)) {
			carl.setNextAction(Action.Still);
			return;
		}
		if (isPressed(Keys.LEFT)) {
			carl.setNextAction(Action.Run);
			carl.setDirection(Direction.Left);
		} else if (isPressed(Keys.RIGHT)) {
			carl.setNextAction(Action.Run);
			carl.setDirection(Direction.Right);
		}
		// show location in map
		if (isJustPressed(Keys.M)) {
			String[] map = world.getMap().getMapString();
			for (int i = 0; i < map.length; i++) {
				for (int j = 0; j < map[i].length(); j++) {
					if (carl.getRow() == i && carl.getCol() == j) {
						System.out.print('A');
					} else {
						System.out.print(map[i].charAt(j));
					}
				}
				System.out.println();
			}
		}
		// --------------------------------------------
		carl.setNextAction(Action.Still);
	}

	public boolean isLastPressTimeOver(int key, float time) {
		double lastTimePress = 0;
		if (key == Keys.X) {
			lastTimePress = lastPressUpTime;
		} else if (key == Keys.Z) {
			lastTimePress = lastPressSpaceTime;
		} else if (key == Keys.A) {
			lastTimePress = lastPressATime;
		}
		return currentTime() - lastTimePress >= time;
	}

	public void updateKeyPress() {
//		if (isJustPressed(Keys.N)) {
//			initGame();
//		}
		if (isJustPressed(Keys.ESCAPE)) {
			if(!carl.isDead()) {
				soundController.changeMenu();
				pause = (pause + 1) % 2;
			}
		}
		if (isJustPressed(Keys.ENTER)) {
			if (onMenu) {
				if (selectMenu == 0) {
					initGame(true);
					onMenu = false;
					soundController.selectMenu();
				} else {
					initGame(false);
					onMenu = false;
					instruction = true;
					pause = 0;
					selectPauseMenu = 0;
					soundController.selectMenu();
					System.out.println("asdf");
				}
			} else if (pause == 1) {
				if (selectPauseMenu == 0) {
					pause = 0;
				} else if (selectPauseMenu == 1) {
					initGame(true);
					selectPauseMenu = 0;
				} else if (selectPauseMenu == 2) {
					initGame(true);
					onMenu = true;
					pause = 0;
					selectPauseMenu = 0;
				}
				soundController.selectMenu();
			} else if (carl.isDead()) {
				soundController.selectMenu();
				initGame(true);
				onMenu = true;
				pause = 0;
				selectPauseMenu = 0;
			} else if (instruction) {
				instruction = false;
				initGame(false);
				onMenu = true;
				pause = 0;
				selectPauseMenu = 0;
				soundController.selectMenu();
			}
		}
	}

	public void menuController() {
		if (onMenu) {
			if (isJustPressed(Keys.UP)) {
				soundController.changeMenu();
				selectMenu = (selectMenu - 1) % totalMenu;
				if(selectMenu < 0) {
					selectMenu += totalMenu;
				}
			}
			if (isJustPressed(Keys.DOWN)) {
				soundController.changeMenu();
				selectMenu = (selectMenu + 1) % totalMenu;
			}
		}
	}
	
	public void pauseMenuController() {
		if(pause == 1) {
			if (isJustPressed(Keys.DOWN)) {
				soundController.changeMenu();
				selectPauseMenu = (selectPauseMenu + 1) % totalPauseMenu;
			}
			if (isJustPressed(Keys.UP)) {
				soundController.changeMenu();
				selectPauseMenu = (selectPauseMenu - 1) % totalPauseMenu;
				if(selectPauseMenu < 0) {
					selectPauseMenu += totalPauseMenu;
				}
			}
		}
	}
	
	public boolean getOnMenu() {
		return onMenu;
	}
	
	public boolean getInstruction() {
		return instruction;
	}
	
	public int getPause() {
		return pause;
	}
	
	public int getSelectMenu() {
		return selectMenu;
	}
	
	public int getSelectPauseMenu() {
		return selectPauseMenu;
	}
}
